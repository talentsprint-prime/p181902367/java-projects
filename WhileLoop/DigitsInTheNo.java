package WhileLoop;

import java.util.Scanner;

public class DigitsInTheNo {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		DigitsInTheNO(num);

	}

	public static void DigitsInTheNO(int num) {
	
		int digit = 0;
		
		while (num > 0) {
			
			digit = num % 10;
			System.out.println(digit);
            num = num / 10;
		
		}
		
		
	}

}
