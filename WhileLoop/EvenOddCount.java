package WhileLoop;

import java.util.Scanner;

public class EvenOddCount {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		countEvenOdd(num);

	}

	public static void countEvenOdd(int no) {
		int evencount = 0, oddcount = 0;

		while (no > 0) {

			int digit = no % 10;

			if (digit % 2 == 0) {
				evencount++;

			} else {
				oddcount++;

			}
			no = no / 10;

		}

		System.out.println("Even digits = "+(evencount));
		
		System.out.println(" Odd digits = "+(oddcount));

	}

}
