package WhileLoop;

import java.util.Scanner;

public class ReverseNo{
public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in);
	
	int num = scan.nextInt();
	System.out.println(reverseNum(num));

}

public static int reverseNum(int no) {

	System.out.println(no);
	int reverse=0;

	while (no > 0) {

		int digit = no % 10;
	
		reverse = reverse+digit;
	
		no = no / 10;

	}
	
	return reverse;

}
}
