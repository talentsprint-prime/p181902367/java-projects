package WhileLoop;

import java.util.Scanner;

public class NaturalNumbers {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		SumOfNaturalNo(num);

	}

	public static void SumOfNaturalNo(int num) {
		int i = 1, sum = 0;
		while (i <= num) {

			sum = sum + i;

			i++;

		}
		System.out.println("The sum of N Natural no is:" + sum);
	}
}
