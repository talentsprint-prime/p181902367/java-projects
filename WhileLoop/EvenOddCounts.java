package WhileLoop;

import java.util.Scanner;

public class EvenOddCounts {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		System.out.println("Even digits = "+(countEven(num)));
		System.out.println("Odd digits = "+countOdd(num));

	}

	public static int countEven(int no) {
		int evencount = 0;

		while (no > 0) {

			int digit = no % 10;

			if (digit % 2 == 0) {
				evencount++;

			}

			no = no / 10;

		}
		return evencount;

	}

	public static int countOdd(int no) {
		int oddcount = 0;
		while (no > 0) {

			int digit = no % 10;

			if (digit % 2 != 0) {
				oddcount++;

			}

			no = no / 10;

		}
		return oddcount;

	}

}
