package WhileLoop;

import java.util.Scanner;

public class ReverseTheNum {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int num = scan.nextInt();
		reverseNum(num);

	}

	public static void reverseNum(int no) {
		System.out.println(no);

		while (no > 0) {

			int num = no % 10;
		
			System.out.print(num);
		
			no = no / 10;

		}

	}
}
