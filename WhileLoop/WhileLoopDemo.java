package WhileLoop;

public class WhileLoopDemo {
	public static void main (String[] args){
		int num=1;
		
		Demo(num);
	}
	public static void Demo(int num){
		while(num<=10){
			System.out.println(num);
			num++;
		}		
	}

}
