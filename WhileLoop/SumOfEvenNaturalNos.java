package WhileLoop;

import java.util.Scanner;

public class SumOfEvenNaturalNos {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		SumOfEvenNos(num);
	}

	public static void SumOfEvenNos(int num) {
	
		int even = 0;
		int s = 1;
	
		while (s <= num) {
			if (s % 2 == 0) {

				even = even + s;

			}
			s++;

		}
		System.out.println(even);

	}
}
