package practice;

import java.util.Arrays;
import java.util.Scanner;

public class SecondLargestNo {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int arr[] = { 7, 8, 14, 9, 15 };
		Arrays.sort(arr);
		System.out.println(arr[arr.length - 2]);
	}

}
