package practice;

import java.util.Scanner;

public class SortAnArrayInAsc {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int arr[] = { 4, 8, 5, 7, 9, 3 };

		SortAsc(arr);

	}

	public static void SortAsc(int arr[]) {

		int temp;

		for (int i = 0; i < arr.length; i++) {

			for (int j = i+1; j < arr.length; j++) {

				if (arr[i] > arr[j]) {

					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;

				}
			}

		}
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}

	}

}
