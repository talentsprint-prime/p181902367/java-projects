package practice;

import java.util.Scanner;

public class SwapWithOutUsingThirdVariabe {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the first No:");
        int x = scan.nextInt();
		
		System.out.println("Enter the Second No:");
		int y = scan.nextInt();

		x = x + y;
		y = x - y;
		x = x - y;

		System.out.println(x);
		System.out.println(y);

	}

}
