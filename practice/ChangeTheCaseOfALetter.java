package practice;

import java.util.Scanner;

public class ChangeTheCaseOfALetter {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String s = scan.nextLine();
		
		System.out.println(Change(s));

	}

	public static String Change(String s) {

		String t = "";
		String v = "";

		for (int i = 0; i < s.length(); i++) {

			char ch = s.charAt(i);
			if (ch == '-') {
				t += "-";
			} else {
				v += s.charAt(i);
			}

		}

		return t+v;

	}

}
