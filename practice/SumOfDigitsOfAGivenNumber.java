package practice;

import java.util.Scanner;

public class SumOfDigitsOfAGivenNumber { 
	
	public static void main(String [] args){
		
		Scanner scan = new Scanner(System.in);
		
		int num = scan.nextInt();
		
		System.out.println(sumOfDigits(num));
	}
		public static int sumOfDigits(int n){
			
		int digit, sum = 0;
			
			while(n>0){
			digit = n%10;
			sum=sum+digit;
			
			n=n/10;
			}
			return sum;
			
		}
		
	}


