package practice;

import java.util.Scanner;

public class SortAnArray {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int arr[] = { 41, 857, 42, 75, 568, 100 };

		int firstlargestno = 0;
		int secondlargestno = 0;

		for (int i = 0; i < arr.length; i++) {
			if (firstlargestno < arr[i]) {

				secondlargestno = firstlargestno;
				firstlargestno = arr[i];

			} else if (secondlargestno < arr[i]) {
				secondlargestno = arr[i];

			}
		}
		System.out.println("FirstLargestNo: " + firstlargestno + " SecondLargestNo: " + secondlargestno);

	}

}
