package practice;

import java.util.Scanner;

public class FindTheSumThatAreDivisibleBy3Or5 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int n = scan.nextInt();

		int arr[] = new int[n];

		for (int i = 0; i < arr.length; i++) {

			arr[i] = scan.nextInt();

		}
		System.out.println(divisible(arr));

	}

	public static int divisible(int arr[]) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			if ((arr[i] % 3 == 0) || (arr[i] % 5 == 0)) {

				sum = sum + arr[i];

			}

		}
		return sum;

	}

}
