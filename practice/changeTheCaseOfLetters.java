package practice;

import java.util.Scanner;

public class changeTheCaseOfLetters {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String s1 = scan.nextLine();

		System.out.println(changeTheCase(s1));

	}

	public static String changeTheCase(String s1) {

		String s = "";

		for (int i = 0; i < s1.length(); i++) {

			char ch = s1.charAt(i);

			if (Character.isUpperCase(ch)) {
				ch = Character.toLowerCase(ch);
			} else {
				ch = Character.toUpperCase(ch);
			}
			s += ch;

		}
		return s;

	}

}
