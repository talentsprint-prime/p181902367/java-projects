package practice;

import java.util.Scanner;

public class ReverseEachWord {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		
		String s = scan.nextLine();
		
		System.out.println(reverse(s));
		
	//	System.out.println(reverse(s));
	}
	
	public static String reverse(String s){
		
		String v="";
		
		for(int i=s.length()-1;i>=0;i--){
			v+=s.charAt(i);
		}
		return v;
		 
	 }
	
		
	}

