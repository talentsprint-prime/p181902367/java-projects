package practice;

import java.util.Scanner;

public class PositivaPercentage {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int arr[] ={3,2,4,5,0,-1,-4,-67};
		
		System.out.println(isPositive(arr));
		
	}
	public static double isPositive(int arr[]){
		
		int s=0;
		int f=0;
		int t=0;
		for(int i=0;i<arr.length;i++){
			
			if(arr[i]>0){
				s+=arr[i];
			}
			else{
				f+=arr[i];
			}
			
			
		}
		t=s+f;
		double result = (s-f)*100/t;
		
		
		return result;
		
		
	}

}
