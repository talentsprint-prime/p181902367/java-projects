package practice;

import java.util.Scanner;

public class palindrome {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num = scan.nextInt();

		int palindrome = Integer.parseInt(isPalindrome(num));

		if (palindrome == num) {
			System.out.println("It is a palindrome");
		} else {
			System.out.println("It is not a palindrome");
		}

	}

	public static String isPalindrome(int num) {

		int rev;
		String s = "";
		while (num > 0) {

			rev = num % 10;
			s = s + rev;
			num = num / 10;

		}
		return s;

	}

}
