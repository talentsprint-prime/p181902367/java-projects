package practice;

import java.util.Scanner;

public class OddOccurances {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int arr[] = { 1, 1, 2, 1, 3, 2, 3, 4, 4, 3, 4, 4, 6 };

		System.out.println(oddOccurance(arr));

	}

	public static int oddOccurance(int arr[]) {

		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			int count = 0;
			if (arr[i] != -1) {
				int e = arr[i];

				for (int j = 0; j < arr.length; j++) {

					if (e == arr[j]) {
						count++;
						arr[j] = -1;
					}
				}

				if (count % 2 != 0) {
					sum += e;

				}
			}
		}

		return sum;

	}

}
