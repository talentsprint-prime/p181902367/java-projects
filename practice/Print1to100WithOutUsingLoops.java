package practice;

import java.util.Scanner;

public class Print1to100WithOutUsingLoops {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num = scan.nextInt();

		getNumber(1);

	}

	public static void getNumber(int num) {

		if (num <= 100) {
			System.out.println(num);
			getNumber(num + 1);
		}

	}

}
