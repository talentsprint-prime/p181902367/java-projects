package practice;

import java.util.Scanner;

public class UniqueElementsIn2DimentionalArray {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int n = scan.nextInt();

		int arr[] = new int[n];
		int arr1[] = new int[n];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = scan.nextInt();
		}
		for (int j = 0; j < arr1.length; j++) {
			arr1[j] = scan.nextInt();
		}
		check(n, arr, arr1);
	}

	public static void check(int n, int arr[], int arr1[]) {
		int unique = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr1.length; j++) {

				if (arr[i] == arr1[j]) {

					unique += arr[i];

				}
			}
			
		}
		System.out.println(unique);

	}

}
