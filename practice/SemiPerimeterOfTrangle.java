package practice;

import java.util.Scanner;

public class SemiPerimeterOfTrangle {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		double a = scan.nextInt();
		double b = scan.nextInt();
		double c = scan.nextInt();

		System.out.println(triangle(a, b, c));

	}

	public static double triangle(double a, double b, double c) {

		double result = (a + b + c) / 2;
		return result;
	}
}
