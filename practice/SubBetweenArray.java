package practice;

import java.util.Scanner;

public class SubBetweenArray {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int n = scan.nextInt();

		int arr[] = new int[n];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = scan.nextInt();
		}
		System.out.println(subOperation(arr));
	}

	public static int subOperation(int arr[]) {

		int sub;
		int res = 0;

		for (int i = 0; i < arr.length-1; i++) {

			sub = arr[i] - arr[i + 1];
			res = res + sub;
		}
		return res;

	}

}
