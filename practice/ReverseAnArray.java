package practice;

import java.util.Scanner;

public class ReverseAnArray {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("How Many No Of Array Elements");
		int n = scan.nextInt();
		int arr[] = new int[n];
		int rev[] = new int[n];
		System.out.println("Enter the array elements");
		for (int i = 0; i < n; i++) {
			arr[i] = scan.nextInt();
		}
		int j = 0;
		System.out.println("Reverse of an array");
		for (int i = n; i > 0; i--, j++) {

			rev[j] = arr[i - 1];
			System.out.print(rev[j] + " ");

		}

	}

}
