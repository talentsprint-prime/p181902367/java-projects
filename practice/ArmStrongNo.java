package practice;

import java.util.Scanner;

public class ArmStrongNo {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num = scan.nextInt();

		if (num == isArmStrongno(num)) {
			System.out.println("It is a ArmStrongNo");
		} else {
			System.out.println("It is Not ArmStrongNO");
		}

	}

	public static int isArmStrongno(int num) {
		int digit;
		int exponent = 3;
		int sum = 0;

		while (num > 0) {

			digit = num % 10;

			sum = (int) (sum + Math.pow(digit, exponent));
			num = num / 10;

		}
		return sum;

	}

}
