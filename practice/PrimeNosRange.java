package practice;

import java.util.Scanner;

public class PrimeNosRange {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num1 = scan.nextInt();
		int num2 = scan.nextInt();

		primeNos(num1, num2);

	}

	public static void primeNos(int num1, int num2) {
		String s = "";
		int count =0;

		for (int i = num1; i <= num2; i++) {
			for (int j = 2; j < i; j++) {

				if (i % j == 0) {
					count = 0;
					break;
				} else {
					count = 1;
				}

			}
			if (count == 1) {
				System.out.print(i + ",");

			}

		}

	}

}
