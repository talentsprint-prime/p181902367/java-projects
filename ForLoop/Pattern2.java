package ForLoop;

import java.util.Scanner;

public class Pattern2 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		pattern(num);

	}

	public static void pattern(int num) {

		for (int r = 1; r <= num; r++) {

			for (int c = num; c >=r;c--) {
				System.out.print("*");
			}
			System.out.println();

		}

	}

}
