package ForLoop;

import java.util.Scanner;

public class PrimeOrNot {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		primeOrNot(num);

	}

	public static void primeOrNot(int num) {
		int count = 0;
		int i;
		for (i = 1; i <= num; i++) {
			if(num%i==0){
				count++;
			}
		}
		if(count==2){
			System.out.println("Prime");
		}
		else{
			System.out.println("Not");
		}
			
		}
}

			