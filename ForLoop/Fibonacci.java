package ForLoop;

import java.util.Scanner;

public class Fibonacci {
	public static void main(String[] args) {

		System.out.println(getFibonacciSeries(1));
	}

	public static String getFibonacciSeries(int num) {

		Scanner scan = new Scanner(System.in);
		num = scan.nextInt();

		int a = 0;
		int b = 1;
		int c;
		String s = "";

		s = s + a;
		s = s + "," + b;

		if (num >= 1 && num <= 40) {
			for (int i = 3; i <= num; i++) {
				c = a + b;
				a = b;
				b = c;
				s = s + "," + c;
			}
		} else {
			return "-1";
		}
		return s;
	}
}
