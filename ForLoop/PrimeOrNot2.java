package ForLoop;

import java.util.Scanner;

public class PrimeOrNot2 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		PrimeOrNot(num);

	}

	public static void PrimeOrNot(int num) {
		int count = 0;

		for (int i = 2; i < num; i++) {
			if (num % i == 0)
				count++;

		}
		if (count == 0) {
			System.out.println("Prime");

		} else {
			System.out.println("Not");
		}
	}
}
