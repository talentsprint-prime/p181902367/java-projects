package ForLoop;

import java.util.Scanner;

public class Factorial {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		System.out.println(Factorial(num));

	}

	public static int Factorial(int num) {
	
		int Fact = 1;
		
		for (int i = 1; i <= num; i++) {
			Fact = Fact * i;
		}
		return Fact;

	}

}
