package ForLoop;

import java.util.Scanner;

public class SecondMaxDigit {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		System.out.println(secondMaxNo(num));

	}

	public static int secondMaxNo(int num) {

		int max = 0;
		int secondmax = 0;
		while (num > 0) {

			int digit = num % 10;

			if (digit > max) {
				secondmax = max;
				max = digit;

			} else if (digit < max && digit > secondmax)
				secondmax = digit;

			num /= 10;
		}

		return secondmax;

	}

}
