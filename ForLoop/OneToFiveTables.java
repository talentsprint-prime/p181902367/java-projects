package ForLoop;

import java.util.Scanner;

public class OneToFiveTables {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		tables(num);

	}

	public static void tables(int num) {
		int multiple;
		for (int j = 1; j <= num; j++) {
			for (int c = 1; c <= 10; c++) {
				multiple = j * c;
				System.out.println(j + " x " + c + " = " + multiple);

			}

		}

	}
}
