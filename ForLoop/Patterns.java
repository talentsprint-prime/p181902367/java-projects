package ForLoop;

import java.util.Scanner;

public class Patterns {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		pattern(num);

	}

	public static void pattern(int lines) {
		
		for (int r = 1; r <= lines; r++) {
			
			for (int c = 1; c <= r; c++) {
				System.out.print("*");
			}
			System.out.println();

		}

	}

}
