package ForLoop;

import java.util.Scanner;

public class DivisibleBy3And5 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num1 = scan.nextInt();
		int num2 = scan.nextInt();
		System.out.println(Divisible(num1, num2));

	}

	public static int Divisible(int num1, int num2) {
		int sum = 0;
		for (int i= num1; i <= num2; i++) {
			if ((i % 3 == 0) || (i % 5 == 0))
				sum = sum + i;

		}
		return sum;

	}

}
