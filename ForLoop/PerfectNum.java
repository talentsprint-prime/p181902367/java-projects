package ForLoop;

import java.util.Scanner;

public class PerfectNum {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		PerfectNum(num);

	}

	public static void PerfectNum(int num) {
		
		int factors, i, sum = 0;
		for (i = 1; i < num; i++) {

			if (num % i == 0)
				sum = sum + i;

		}
		System.out.println(sum);
	
		if (sum == num) {
			System.out.println("It Is A Perfect Number");
		}

	}

}
