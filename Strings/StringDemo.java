package Strings;

import java.util.Scanner;

public class StringDemo {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String str = scan.next();

		System.out.println(str.length());
		System.out.println(str.indexOf("v"));
		System.out.println(str.toUpperCase());
		System.out.println(str.toLowerCase());
		System.out.println(str.trim());
		System.out.println(str.charAt(3));

		String str1 = scan.next();
		System.out.println(str.concat(str1));

	}

}
