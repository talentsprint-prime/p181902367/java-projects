package Strings;

import java.util.Scanner;

public class getSwapCase {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the string");
		String str = scan.nextLine();

		System.out.println(changeTheCase(str));

	}

	public static String changeTheCase(String str) {

		String s = "";

		for (int i = 0; i < str.length(); i++) {

			char c = str.charAt(i);

			if (Character.isUpperCase(c)) {
				c = Character.toLowerCase(c);

			} else if (Character.isLowerCase(c)) {
				c = Character.toUpperCase(c);
			}

			s = s + c;

		}
		return s;

	}

}
