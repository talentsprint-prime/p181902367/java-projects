package Strings;

import java.util.Scanner;

public class Palindrome {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String s = scan.next();
		System.out.println(Palindrome(s));

		System.out.println("Palindrome is" + isPalindrome(s));

		if (isPalindrome(s))
			System.out.println("It is a palindrome");
		else
			System.out.println("It is not a palindrome");

	}

	public static String Palindrome(String s) {
		String p = "";

		for (int i = s.length() - 1; i >= 0; i--) {
			p = p + s.charAt(i);
		}
		return p;

	}

	public static boolean isPalindrome(String s) {

		String rev = Palindrome(s);
		if (s.equals(rev)) {
			return true;
		} else
			return false;
	}

}
