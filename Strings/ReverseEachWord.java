package Strings;

import java.util.Scanner;

public class ReverseEachWord {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the String:");

		String str = scan.nextLine();
		System.out.println(reverseWord(str));

	}

	public static String reverseWord(String str) {

		String word = "";
		String s[] = str.split(" ");

		for (int i = 0; i < s.length; i++) {
			String s1 = s[i];
			
			for (int j = s1.length() - 1; j >= 0; j--) {
				word = word + s1.charAt(j);
			}
			
			word=word+" ";

		}
		return word;
	}

}
