package Strings;

import java.util.Scanner;

public class ReverseTheName {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter string");

		String s = scan.next();

		System.out.println("Reverse string is:" + reverseString(s));

	}

	public static String reverseString(String s) {

		String reverse = "";
		for (int i = s.length() - 1; i >= 0; i--) {
			reverse = reverse + s.charAt(i);

		}
		return reverse;
	}

}
