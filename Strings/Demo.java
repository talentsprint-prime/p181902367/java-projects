package Strings;

import java.util.Scanner;

public class Demo {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the first name");
		String name1 = scan.next();

		System.out.println("Enter the second name");
		String name2 = scan.next();

		System.out.println(name1 +" "+ name2);
	}

}
