package BasicProgramms;

import java.util.Scanner;

public class AverageInMethods {
	public static void main (String[] args){
		
		Scanner scan=new Scanner(System.in);
		double num1,num2,num3;
		
		System.out.println("Enter the 3 Numbers:");
		
		num1= scan.nextDouble();
		num2=scan.nextDouble();
		num3=scan.nextDouble();
		
		double avg= average(num1,num2,num3);
		System.out.println("The Average is:"+avg);
		
	}
		

	public static double average(double num1, double num2, double num3) {
		double avg;
		avg=(num1+num2+num3)/3;
		
		return avg;
	}

}
