package BasicProgramms;

import java.util.Scanner;

public class SpeedConversion {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		double distance, hours, minutes, seconds;
		System.out.println("Enter the distance:");
		distance = scan.nextDouble();
		
		System.out.println("Enter the hours:");
		hours = scan.nextDouble();
		
		System.out.println("Enter the minutes:");
		minutes = scan.nextDouble();
		
		System.out.println("Enter the seconds:");
		seconds = scan.nextDouble();
		
		double speedinMperS, speedinKmperS, speedinMilesperH, totalsec;
		
		totalsec = (hours * 60 * 60) + (minutes * 60) + (seconds);
		speedinMperS = distance / totalsec;
		speedinKmperS = ((distance/1000)/(totalsec/3600));
		speedinMilesperH = (distance / 1609) / (totalsec / 3600);
		
		System.out.println("Speed in Meters per second " + speedinMperS);
		System.out.println("Speed in kilometer per Hour " + speedinKmperS);
		System.out.println("Speed in Miles oer hour " + speedinMilesperH);

	}

}
