package BasicProgramms;

import java.util.Scanner;

public class MinutesConversion {
	public static void main(String[] args) {

		int years, days, hours, minutes;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the minutes:");

		int min;
        min = scan.nextInt();
		
        years = min / (365 * 24 * 60);
		days = (min / 60 / 24) % 365;
		hours = (min % (24 * 60)) / 60;
		minutes = ((min % (24 * 60)) % 60);

		System.out.println(years + " Years " + days + " days " + hours + " hours " + minutes + " minutes ");

	}

}
