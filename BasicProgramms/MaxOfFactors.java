package BasicProgramms;

import java.util.Scanner;

public class MaxOfFactors {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num = scan.nextInt();
		System.out.println(maxOfNos(num));

	}

	public static int maxOfNos(int num) {

		int flag = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0) {
				flag = i;
			}

		}
		return flag;

	}
}
