package BasicProgramms;

import java.util.Scanner;

public class HCF {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num1 = scan.nextInt();
		int num2 = scan.nextInt();
		int num3 = scan.nextInt();
		System.out.println(HCF(num1, num2, num3));

	}

	public static int HCF(int num1, int num2, int num3) {

		int hcf = 0;
		for (int i = 1; i <= num1; i++) {
			if ((num1 % i == 0) && (num2 % i == 0) && (num3 % i == 0)) {
				hcf = i;
			}
		}
		return hcf;

	}

}
