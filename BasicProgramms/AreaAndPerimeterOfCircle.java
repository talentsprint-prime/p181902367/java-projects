package BasicProgramms;

import java.util.Scanner;

public class AreaAndPerimeterOfCircle {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double area, perimeter, radius;

		System.out.println("Enter the radius:");
		radius = scan.nextDouble();

		area = AreaOfCircle(radius);
		System.out.println("Area of the circle:" + area);

		perimeter = AreaOfPerimeter(radius);
		System.out.println("Perimeter of the circle:" + perimeter);
	}

	public static double AreaOfCircle(double radius) {

		double areaOfTriangle;
		areaOfTriangle = 3.14 * radius * radius;

		return areaOfTriangle;

	}

	public static double AreaOfPerimeter(double radius) {

		double areaOfPerimeter;
		areaOfPerimeter = 2 * 3.14 * radius;

		return areaOfPerimeter;
	}
}
