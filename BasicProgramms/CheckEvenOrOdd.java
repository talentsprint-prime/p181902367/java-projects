package BasicProgramms;

import java.util.Scanner;

public class CheckEvenOrOdd {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter The Number:");

		boolean result;
		int num = scan.nextInt();

		result = EvenOrOdd(num);

		if (result == true) {
			System.out.println("The Number Is Even");
		} else {
			System.out.println("The Number Is Odd");
		}

	}

	public static boolean EvenOrOdd(int no) {

		if (no % 2 == 0) {
			return true;

		} else {
			return false;

		}
	}

}
