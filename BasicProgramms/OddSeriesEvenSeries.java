package BasicProgramms;

import java.util.Scanner;

public class OddSeriesEvenSeries {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter The Number:");

		int num = scan.nextInt();

		getEvenOddSeries(num);

	}

	public static void getEvenOddSeries(int num1) {

		if (num1 % 2 == 0) {
			for (int i = 0; i < num1 * 2;) {

				System.out.println(i + " ");
				i = i + 2;

			}
		} else {
			for (int i = 1; i < num1 * 2;) {
				System.out.print(i + " ");
				i += 2;

			}

		}

	}
}
