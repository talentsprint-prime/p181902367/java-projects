package Prime;

public class Twinprimes {
	public static void main(String[] args) {
		int num1 = 1;
		int num2 = 200;
		System.out.println(twinPrimes(num1, num2));

	}

	public static String twinPrimes(int start, int limit) {
		if (start <= 0 || limit <= 0) {
			return "-1";
		} else if (start >= limit) {
			return "-2";
		}
		String res = "";
		for (int i = start; i <= limit; i++) {
			if (isPrime(i)) {
				if (isPrime(i + 2)) {
					res += i + ":" + (i + 2) + ",";
				}
			}
		}
		return res.isEmpty() ? "-3" : res.substring(0, res.length() - 1);
	}

	public static boolean isPrime(int num) {
		int count = 0;
		if (num == 1) {
			num++;
		} else {
			for (int i = 1; i <= num / 2; i++) {
				if (num % i == 0) {
					count++;
				}
			}
		}
		return count == 1;
	}
}
	


