package Prime;

public class LuckyNumber {
	public static void main(String[] args) {
		String date = "15-March-2016";
		System.out.println(getLuckyNumber(date));
	}

	public static int getLuckyNumber(String date) {
		String[] partsOfDate = date.split("-");
		int dd = Integer.parseInt(partsOfDate[0]);
		int mon = convertMMMtoMM(partsOfDate[1]);
		int year = Integer.parseInt(partsOfDate[2]);

		int sumOfDigits = getSumOfDigits(dd);
		sumOfDigits += getSumOfDigits(mon);
         sumOfDigits += getSumOfDigits(year);
         
         while(sumOfDigits>10){
        	 sumOfDigits += getSumOfDigits(sumOfDigits);
         }
         return sumOfDigits;
	}

	public static int convertMMMtoMM(String mon) {
		String months = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();

		return ((months.indexOf(mon) / 3) + 1);
	}

	public static int getSumOfDigits(int num) {
		int sum=0;
		while(num>0){
			sum +=num%10;
			num =num/10;
		}
		return sum;
		

	}
}