package Prime;

import java.util.Scanner;

public class AlphabetPuzzle {

	static int puzzCount = 0;
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		String res = "";
		while (true) {

			char[][] puzz = new char[5][5];
			int x = 0, y = 0;
			int[] space = new int[2];

			puzz = getInputPuzzle();

			if (puzz[0][0] == 'Z') {
				break;
			}

			space = getSpaceLoc(puzz);
			x = space[0];
			y = space[1];

			puzz = updatePuzzle(puzz, x, y);
			res = displayPuzzle(puzz, res);

		}
		System.out.println(res);

	}

	static int[] getSpaceLoc(char[][] puzz) {
		int space[] = new int[2];
		for (int i = 0; i < puzz.length; i++) {
			for (int j = 0; j < puzz[i].length; j++) {

				if (puzz[i][j] == ' ') {
					space[0] = i;
					space[1] = j;
				}
			}

		}
		return space;

	}

	public static String displayPuzzle(char[][] puzz, String res) {
		res += "\n";
		res += "Puzzle #" + (++puzzCount) + ":\n";

		if (puzz[0][0] == '*') {
			res += "This puzzle has no final configuration.";
		} else {
			for (int i = 0; i < puzz.length; i++) {

				for (int j = 0; j < puzz[i].length; j++) {
					res += puzz[i][j] + " ";
				}
				res += "\n";

			}
		}
		return res;

	}

	static char[][] updatePuzzle(char[][] puzz, int x, int y) {
		boolean cmdEnd = false, invMove = false;

		while (!cmdEnd) {
			char[] cmd = scan.nextLine().toCharArray();

			for (int i = 0; i < cmd.length; i++) {
				if (cmd[i] == '0') {
					cmdEnd = true;
					break;
				} else if (cmd[i] == 'A' && !invMove) {
					if (x == 0) {
						invMove = true;
					} else {
						puzz[x][y] = puzz[x - 1][y];
						puzz[x - 1][y] = ' ';
						x -= 1;

					}
				} else if (cmd[i] == 'B' && !invMove) {
					if (x == 4) {
						invMove = true;
					} else {
						puzz[x][y] = puzz[x + 1][y];
						puzz[x + 1][y] = ' ';
						x += 1;
					}
				} else if (cmd[i] == 'L' && !invMove) {
					if (y == 0) {
						invMove = true;
					} else {
						puzz[x][y] = puzz[x][y - 1];
						puzz[x][y - 1] = ' ';
						y -= 1;
					}
				} else if (cmd[i] == 'R' && !invMove) {
					if (y == 4) {
						invMove = true;
					} else {
						puzz[x][y] = puzz[x][y + 1];
						puzz[x][y + 1] = ' ';
						y += 1;
					}
				} else {
					invMove = true;
				}

			}

		}
		if (invMove == true) {
			puzz[0][0] = '*';
		}

		return puzz;

	}

	static char[][] getInputPuzzle() {
		String s = null;
		s = scan.nextLine();
		char puzzle[][] = new char[5][5];
		if (s.equals("Z")) {
			puzzle[0][0] = 'Z';
		} else {
			if (s.length() == 4)
				s += ' ';
			puzzle[0] = s.toCharArray();

			for (int i = 1; i < 5; i++) {
				s = scan.nextLine();
				if (s.length() == 4)
					s += ' ';
				puzzle[i] = s.toCharArray();

			}

		}
		return puzzle;
	}

}
