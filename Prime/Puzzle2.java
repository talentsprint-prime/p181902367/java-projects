package Prime;

import java.util.Scanner;

public class Puzzle2 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		char[][] puzzle = new char[5][5];
		getInput(puzzle);
		display(puzzle);

		int[] space = findSpace(puzzle);

		while (!isWin(puzzle)) {
			System.out.println();
			System.out.println("Enter your move: A - Above B - below R - Right L - Left");

			char move = scan.next().charAt(0);
			char[][] puzzl;

			if (move == 'A' || move == 'B' || move == 'R' || move == 'L') {
				if (isValidMove(puzzle, move)) {
					puzzl = getSwap(puzzle, move);

					display(puzzl);
				} else {
					System.out.println("There is no final configuration");
					break;
				}

			} else {
				System.out.println("No further move");

				break;
			}
		}

	}

	static char[][] getInput(char[][] puzzle) {
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {

				puzzle[i][j] = sc.next().charAt(0);

			}
		}

		return puzzle;
	}

	private static void display(char[][] puzzle) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {

				System.out.print(puzzle[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static int[] findSpace(char[][] puzzle) {
		int space[] = new int[2];
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle[i].length; j++) {
				if (puzzle[i][j] == '-') {
					space[0] = i;
					space[1] = j;
					return space;

				}

			}
		}
		return null;
	}

	public static boolean isValidMove(char[][] puzzle, char move) {
		int[] position = findSpace(puzzle);
		int row = position[0];
		int column = position[1];
		
		if (move == 'A')
			return row - 1 >= 0;
		else if (move == 'B')
			return row + 1 < puzzle.length;
		else if (move == 'L')
			return column - 1 >= 0;
		else if (move == 'R')
			return column + 1 < puzzle[0].length;
		else {
			System.out.println("Invalid move");
		}
		return false;

	}

	public static char[][] getSwap(char[][] puzzle, char move) {
		int[] currentPosition = findSpace(puzzle);
		int row = currentPosition[0];
		int column = currentPosition[1];
		char temp;
		if (move == 'A') {
			temp = puzzle[row][column];
			puzzle[row][column] = puzzle[row - 1][column];
			puzzle[row - 1][column] = temp;
		} else if (move == 'B') {
			temp = puzzle[row][column];
			puzzle[row][column] = puzzle[row + 1][column];
			puzzle[row + 1][column] = temp;
		} else if (move == 'L') {
			temp = puzzle[row][column];
			puzzle[row][column] = puzzle[row][column - 1];
			puzzle[row][column - 1] = temp;
		} else if (move == 'R') {
			temp = puzzle[row][column];
			puzzle[row][column] = puzzle[row][column + 1];
			puzzle[row][column + 1] = temp;
		}

		return puzzle;

	}

	public static boolean isWin(char[][] puzzle) {

		String winString = "ABCDEFGHIJKLMNOPQRSTUVWX";
		String puzz = "";

		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle[i].length; j++) {
				puzz = puzz + puzzle[i][j];
			}
		}
		String p = puzz.substring(1);
		String p1 = puzz.substring(0, puzz.length() - 1);

		if (winString.equals(p) || winString.equals(p1)) {
			return true;
		}
		return false;

	}

}
