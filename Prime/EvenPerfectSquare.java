package Prime;
      
import java.util.Scanner;

public class EvenPerfectSquare {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter first number:");
		int n1 = scan.nextInt();
		System.out.println("Enter second number:");
		int n2 = scan.nextInt();
		System.out.println(evenPerfectSquare(n1, n2));

	}

	public static String evenPerfectSquare(int n1, int n2) {
		String str = "";
		for (int i = n1; i <= n2; i++) {
			if (allDigitsEven(i) && isPerfectSquare(i)) {
				str += i + " ";
			}
		}
		return str;

	}

	public static boolean isPerfectSquare(int num) {
		return (Math.sqrt(num) % 1 == 0);
	}

	public static boolean allDigitsEven(int num) {

		int digit = 0;
		int count = 0;
		while (num > 0) {

			digit = num % 10;
			if (digit % 2 == 0)
				count++;
			num /= 10;
		}
		if (count == 4)
			return true;
		else
			return false;
	}

}
