package Prime;

import static org.junit.Assert.*;

import org.junit.Test;

public class Puzzle2Test {

	@Test
	public void testGetInput() {
		char[][] input = new char[5][5];
		char[][] inputRes = new char[5][5];
		char alpha = 'A';
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++) {
				input[i][j] = alpha++;

			}
		}                               
		inputRes = Puzzle2.getInput(inputRes);
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++) {
				assertEquals(input[i][j], inputRes[i][j]);
			}
		}

	}

	@Test
	public void testFindSpace() {
		char[][] input = new char[5][5];
		int res[] = new int[2];
		char alpha = 'A';

		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++) {
				input[i][j] = alpha++;
			}
		}
		input[2][3] = '-';

		res = Puzzle2.findSpace(input);
		assertEquals(2, res[0]);
		assertEquals(3, res[1]);
	}

	@Test
	public void testGetSwap() {
		
		char[][] input = { { 'T', 'R', 'G', 'S', 'J' }, { 'X', 'D', 'O', 'K', 'I' }, { 'M', '-', 'V', 'L', 'N' },
				{ 'W', 'P', 'A', 'B', 'E' }, { 'U', 'Q', 'H', 'C', 'F' }, };
		
		char[][] inputRes = { { 'T', 'R', 'G', 'S', 'J' }, { 'X', 'O', 'K', 'L', 'I' }, { 'M', 'D', 'V', 'B', 'N' },
				{ 'W', 'P', '-', 'A', 'E' }, { 'U', 'Q', 'H', 'C', 'F' }, };

		char move = 'B';
		char inputResP[][] = Puzzle2.getSwap(inputRes, move);

		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++) {
				assertEquals(inputRes[i][j], inputResP[i][j]);
			}
		}
	}

}
