package Prime;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class crossword2 {
	static char puzzle[][] = new char[6][7];

	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				puzzle[i][j] = scan.next().charAt(0);
			}
		}
		displayPuzzle(puzzle);
		int[][] n = getNumbered(puzzle);
		List<String> a = getWordsAcross(puzzle, getNumbered(puzzle));
		// List<String> b = getWordTopToDown(puzzle, getNumbered(puzzle));
		for (int i = 0; i < n.length; i++) {
			for (int j = 0; j < n[i].length; j++) {

				System.out.print(n[i][j] + " ");

			}
			System.out.println();
		}
		for (String s : a) {
			System.out.println(s);
		}
		// for (String s1 : b) {
		// System.out.println(s1);
		// }

	}

	public static void displayPuzzle(char[][] puzzle) {

		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle[i].length; j++) {
				System.out.print(puzzle[i][j]);
			}
			System.out.println();

		}

	}

	public static int[][] getNumbered(char[][] puzzle) {
		int count = 0;
		int[][] numcross = new int[puzzle.length][puzzle[0].length];
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle[i].length; j++) {
				if (i == 0 || j == 0 && puzzle[i][j] != '*') {
					numcross[i][j] = ++count;
				} else if (puzzle[i][j] != '*' && (puzzle[i - 1][j] == '*' || puzzle[i][j - 1] == '*')) {
					numcross[i][j] = ++count;
				}

			}
		}
		return numcross;

	}

	public static List<String> getWordsAcross(char[][] puzzle, int[][] getNumbered) {
		List<String> across = new ArrayList<String>();
		String s = "";
		int count = 0;
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle[i].length; j++) {
				if (s.length() == 0) {
					count = getNumbered[i][j];
				}
				if (puzzle[i][j] != '*') {
					s = s + puzzle[i][j];
				} else {
					if (count != 0) {
						s = count + " " + s;
						across.add(s);
					}
					s = "";

				}
			}
		}
		return across;

	}

	/*
	 * public static List<String> getWordTopToDown(char[][] puzzle, int[][]
	 * getNumbered) { List<String> across = new ArrayList<String>(); String s =
	 * ""; int count = 0; for (int j = 0; j < 7; j++) { for (int i = 0; i < 6;
	 * i++) { if (s.length() == 0) { count = getNumbered[i][j]; } if
	 * (puzzle[i][j] != '*') { s = s + puzzle[i][j]; } else { if (count != 0) {
	 * s = count + " " + s; across.add(s); } s = "";
	 * 
	 * } } } return across;
	 * 
	 * }
	 */

}
