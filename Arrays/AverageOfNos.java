package Arrays;

import java.util.Scanner;

public class AverageOfNos {

	public static void main(String[] args) {

		int arr[] = new int[5];
		Scanner scan = new Scanner(System.in);

		for (int i = 0; i < 5; i++)
			arr[i] = scan.nextInt();

		System.out.println(getavg(arr));

	}

	public static double getavg(int arr[]) {

		int sum = 0;
		double avg = 0;

		for (int i = 0; i < arr.length; i++)
			sum = sum + arr[i];

		return avg = sum / arr.length;

	}

}
