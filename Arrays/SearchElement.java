package Arrays;

import java.util.Scanner;

public class SearchElement {
	public static void main(String[] args) {

		int arr[] = new int[5];
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the Elements:");

		for (int i = 0; i < 5; i++)
			arr[i] = scan.nextInt();

		System.out.println("SearchElement:");
		int searchelement = scan.nextInt();

		int result = isFound(arr, searchelement);

		if (result != 0)
			System.out.println("Element is in Position" + result);
		else
			System.out.println("Element is not found");

	}

	public static int isFound(int arr[], int searchelement) {

		for (int i = 1; i < arr.length; i++) {

			if (arr[i] == searchelement)
				return i;
		}
		return 0;
	}
}
