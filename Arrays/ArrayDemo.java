package Arrays;

import java.util.Scanner;

public class ArrayDemo {
	public static void main(String[] args) {

		int arr[] = new int[5];

		Scanner scan = new Scanner(System.in);

		for (int i = 0; i < 5; i++)
			arr[i] = scan.nextInt();

		Nos(arr);

	}

	public static void Nos(int arr[]) {

		for (int i = 0; i < 5; i++)
			System.out.print(arr[i] + " ");
	}

}
