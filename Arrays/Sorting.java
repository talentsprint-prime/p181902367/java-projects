package Arrays;

import java.util.Scanner;

public class Sorting {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the size:");

		int size = scan.nextInt();
		int arr[] = new int[size];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = scan.nextInt();
		}

		System.out.println("Before sorting:");
		printArray(arr);

		System.out.println("After Sorting:" + ArraySort(arr));

	}

	private static void printArray(int[] arr) {

		for (int i = 0; i < arr.length; i++)
			System.out.print(arr[i] + " ");

	}

	public static int[] ArraySort(int arr[]) {

		for (int i = 0; i < arr.length; i++) {
			for (int j = i; j < arr.length; j++) {

				if (arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}

			}

		}
		return arr;

	}

}
