package Arrays;

import java.util.Scanner;

public class CheckMatrixIsIdentity {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the matrix elements:");

		int matrix[][] = new int[3][3];

		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix.length; c++) {

				matrix[r][c] = scan.nextInt();

			}
		}
		System.out.println(identityMatrix(matrix));

	}

	public static String identityMatrix(int matrix[][]) {

		String s = "";
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix.length; c++) {

				if (r == c) {  
					if (matrix[r][c] != 1) {
						return "It is not identity matrix";
					}

				} else if (matrix[r][c] != 0) {
					return "It is not identity matrix";

				} else
					return "It is identity matrix";

			}

		}
		return s;
	}

}
