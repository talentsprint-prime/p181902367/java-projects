package ByUsingMethods;

import java.util.Scanner;

public class MinutesConversion {
	public static void main(String[] args) {

		int years, days, hours, minutes = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the minutes:");

		int minute;
		minute = scan.nextInt();
		System.out.println(Years(minute) + " Years " + Days(minute) + " days " + Hours(minute) + " hours "
				+ Minutes(minute) + " minutes ");

	}

	public static int Years(int minutes) {
		int years;
		years = minutes / (365 * 24 * 60);
		return years;
	}

	public static int Days(int minutes) {
		int days;
		days = (minutes / 60 / 24) % 365;
		return days;
	}

	public static int Hours(int minutes) {
		int hours;
		hours = (minutes % (24 * 60)) / 60;
		return hours;
	}

	public static int Minutes(int minutes) {
		int minute;
		minute = ((minutes % (24 * 60)) % 60);
		return minute;
	}

}
