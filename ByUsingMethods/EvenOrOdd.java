package ByUsingMethods;

import java.util.Scanner;

public class EvenOrOdd {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		System.out.println(evenoroddcheck(num));

	}

	public static String evenoroddcheck(int no) {
		String s = "";
		if (no % 2 == 0)
			s = "EVEN";
		else
			s = "odd";
		return s;
	}
}
