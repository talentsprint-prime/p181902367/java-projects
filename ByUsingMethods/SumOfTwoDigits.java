package ByUsingMethods;

import java.util.Scanner;

public class SumOfTwoDigits {
	public static void main(String[] args) {
		
		System.out.println("Enter the number:");
		
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		System.out.println(getSumOfDigits(num));
	}

	
	public static int getSumOfDigits(int num) {
		
		int result = 0;
		
		if (num > 10 && num < 100)
			result = (num % 10) + (num / 10);
		return result;

	}
}