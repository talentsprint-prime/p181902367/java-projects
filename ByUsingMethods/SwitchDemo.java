package ByUsingMethods;

import java.util.Scanner;

public class SwitchDemo {
	public static void main(String[] args) {
		int ch;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the case:");
		ch = scan.nextInt();

		System.out.println(+conversion(ch));
	}

	public static double conversion(int ch) {
		Scanner scan = new Scanner(System.in);
		double result = 0;

		switch (ch) {
		case 0:
			System.out.println("Fahrenheit to Celsius");
			int farenhiet = scan.nextInt();
			result = (farenhiet - 32) / 1.8;
			break;

		case 1:
			System.out.println("Celsius to Fahrenheit");
			int celsius = scan.nextInt();
			result = (1.8 * celsius) + 32;
			break;

		}
		return result;
	}

}
