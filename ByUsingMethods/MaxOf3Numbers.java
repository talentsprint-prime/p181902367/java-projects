package ByUsingMethods;

import java.util.Scanner;

public class MaxOf3Numbers {
	public static void main(String[] args) {
		System.out.println("Enter the Numbers:");
		Scanner scan = new Scanner(System.in);

		int num1 = scan.nextInt();
		int num2 = scan.nextInt();
		int num3 = scan.nextInt();

		System.out.println("Maximum No Is:" + (Max(num1, num2, num3)));
	}

	public static int Max(int num1, int num2, int num3) {

		int max = num1;

		if (num2 > max)
			max = num2;

		if (num3 > max)
			max = num3;

		return max;

	}

}
