package ByUsingMethods;

import java.util.Scanner;

public class ByUsingMethods {
	public static void main(String[] args) {

		System.out.println("Enter two numbers");
		Scanner scan = new Scanner(System.in);
		float n1 = scan.nextFloat();
		float n2 = scan.nextFloat();

		float s = addition(n1, n2);
		System.out.println("Addition of " + n1 + " and " + n2 + " is " + s);

		System.out.println("Enter two numbers");
		float s1 = scan.nextFloat();
		float s2 = scan.nextFloat();

		float sum = addition(s1, s2);
		System.out.println("Addition of " + s1 + " and " + s2 + " is " + s);

	}

	public static float addition(float n1, float n2) {
		float sum = n1 + n2;
		return sum;
	}

}
