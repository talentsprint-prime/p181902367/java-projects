package ByUsingMethods;

import java.util.Scanner;

public class FiveDigitOrNot {
	private static Object String;

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the number:");

		int num = scan.nextInt();

		System.out.println(fiveDigitNO(num));

	}

	public static String fiveDigitNO(int number) {
		String s = "";
		if ((number >= 10000) && (number <= 99999)) {

			s = "TRUE";
		} else {

			s = "FALSE";

		}

		return s;

	}
}
